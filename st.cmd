#!/usr/bin/env iocsh.bash

require(fs725)
require(fs725sync)
require(cntpstats)
require(essioc)

epicsEnvSet("IOCNAME", "ECDC-EVG:TS-FS725-01")
epicsEnvSet("P", "ECDC-EVG:TS-FS725-01:")
epicsEnvSet("IPADDR", "ecdc-evg-rb-moxa")
#epicsEnvSet("IPADDR", "172.30.38.25")
epicsEnvSet("PORT", "4001")
epicsEnvSet("EVG", "ECDC-B02:TS-EVG-01:")
epicsEnvSet("EVR", "Labs-ECDC:TS-EVR-11:")
epicsEnvSet("SYS", "$(P)")

iocshLoad("$(fs725_DIR)/fs725.iocsh", "P=$(P), IPADDR=$(IPADDR), PORT=$(PORT)")
iocshLoad("$(fs725sync_DIR)/fs725sync.iocsh","P=$(P),  EVR=$(EVR), EVG=$(EVG)")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV=)")

#dbLoadRecords("sources.db","SYS=,DEV=${DEV}")
#dbLoadRecords("tracking.db","SYS=,DEV=${DEV}")

iocInit()

#EOF
